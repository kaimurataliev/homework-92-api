const express = require('express');
const app = express();
const expressWs = require('express-ws')(app);
const cors = require('cors');

const port = 8000;

app.use(cors());

const clients = {};
let pixelArray = [];

app.ws('/pixels', (ws, req) => {
    const id = req.get('Sec-WebSocket-Key');
    clients[id] = ws;

    console.log('Number of active connections', Object.values(clients).length);

    ws.send(JSON.stringify({
        type: 'NEW_ARRAY',
        array: pixelArray
    }));


    ws.on('message', (msg) => {
        let decodedMessage;

        try {
            decodedMessage = JSON.parse(msg);
        } catch (e) {
            return ws.send(JSON.stringify({
                type: 'ERROR',
                message: 'Message is not JSON'
            }))
        }

        switch (decodedMessage.type) {

            case 'PIXEL_ARRAY':
                pixelArray = pixelArray.concat(decodedMessage.array);
                Object.values(clients).forEach(client => {
                    client.send(JSON.stringify({
                        type: 'NEW_ARRAY',
                        array: pixelArray
                    }))
                });
                break;

            default:
                return ws.send(JSON.stringify({
                    type: 'ERROR',
                    message: 'Unknown message type'
                }));
        }
    });

    ws.on('close', (msg) => {
        delete clients[id];
        console.log('Client disconnected');
        console.log('Number of active connections', Object.values(clients).length);
    })
});

app.listen(port, () => {
    console.log('Server started!! :D');
});
